import style_api as api

content = api.load_image('... .jpg', max_size=150)
style = api.load_image('... .jpg', max_size=150)

img = api.style_transfer(content_image=content, style_image=style, 
                         content_layer_ids=[4], style_layer_ids=[1, 2, 3], 
                         num_iterations=5)

# img и есть конечная фотка