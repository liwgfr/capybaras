import face_recognition
from PIL import Image


def recognize(file):
    const = 1.5
    image = face_recognition.load_image_file("./preparation/" + str(file) + ".jpg")
    face_locations = face_recognition.face_locations(image)
    top, right, bottom, left = face_locations[0]
    i = Image.fromarray(image).crop(box=(left/const, top/const, right, bottom*const))
    i.save("./finished/" + str(file) + ".png")
