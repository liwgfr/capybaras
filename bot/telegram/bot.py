from logging import error


import os
from telegram import Bot
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler

from config import TOKEN
from face.recognising import recognize


def start(bot, update):
    bot.sendMessage(update.message.chat_id, 'Отправь мне фотку и я сделаю карикатуру!')


def echo(bot, update):
    update.message.reply_text(update.message.text)


def button(bot, update):
    query = update.callback_query

    file_name = '{}.jpg'.format(query.message.chat_id)
    Bot.send_photo(bot, query.message.chat_id, photo=open(file_name, 'rb'))

    os.remove(file_name)


def photo(bot, update):
    file_id = update.message.photo[-1].file_id
    new_file = bot.getFile(file_id)

    file_name = '../../face/preparation/{}.jpg'.format(
        update.message.chat_id)
    new_file.download(file_name)
    print(update.message.chat_id)
    recognize(update.message.chat_id)


def start_bot():
    REQUEST_KWARGS = {
        'proxy_url': 'http://181.191.98.123:8080/'
    }

    updater = Updater(TOKEN, request_kwargs=REQUEST_KWARGS)
    dp = updater.dispatcher
    dp.add_handler(CommandHandler('start', start))
    dp.add_handler(CommandHandler('help', help))
    dp.add_handler(MessageHandler(Filters.text, echo))

    updater.dispatcher.add_handler(CallbackQueryHandler(button))

    photo_handler = MessageHandler(Filters.photo, photo)
    dp.add_handler(photo_handler)

    dp.add_error_handler(error)
    updater.start_polling()
    updater.idle()
