import logging

from bot.telegram.bot import start_bot
from bot.vk.vkbot import startvk


def configure_logging():
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)


def main():
    configure_logging()
    startvk()
    start_bot()
