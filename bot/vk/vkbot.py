import vk_api
import time
import requests
import config
import style_api as api
from os import listdir
#from random import choice
from vk_api import VkUpload
from PIL import Image
import numpy as np
from numpy.random import choice

vk = vk_api.VkApi(token=config.tokenvk)
vk._auth_token()

vkuser = vk_api.VkApi(token=config.tokenuser)
vkuser._auth_token()


def dowload_image(url, name):
    with open(name, 'wb') as f:
        req = requests.get(url)
        f.write(req.content)


def startvk():
    group_id = 180731264
    longPoll = vk.method("groups.getLongPollServer", {"group_id": group_id})
    server, key, ts = longPoll['server'], longPoll['key'], longPoll['ts']
    print('start')
    while True:
        try:
            longPoll = requests.post('%s' % server, data={'act': 'a_check',
                                                          'key': key,
                                                          'ts': ts,
                                                          'wait': 60}).json()

            if longPoll['updates'] and len(longPoll['updates']) != 0:
                for update in longPoll['updates']:
                    if update['type'] == 'message_new':
                        print(update)

                        obj = update['object']
                        id = obj['from_id']
                        text = obj['text']
                        if text.lower() == "начать":
                            vk.method('messages.send',
                                      {'user_id': id, 'message': "Я вас категорически приветствую, пришлите фото",
                                       'random_id': obj['random_id']})
                        elif len(obj['attachments']) == 1 and obj['attachments'][0]['type'] == 'photo':

                            photo_url = obj['attachments'][0]['photo']['sizes'][1]['url']
                            dowload_image(photo_url, '../../face/preparation/' + photo_url.split('/')[-1])

                            vk.method('messages.send',
                                      {'user_id': id, 'message': "Фото принято, ожидайте...",
                                       'random_id': obj['random_id']})

                            print('../../face/preparation/' + photo_url.split('/')[-1])
                            print('../../face/anime/' + choice(listdir('../../face/anime/')))

                            api.load_vgg('../../vgg16/')
                            api.vgg16.maybe_download()

                            content = api.load_image('../../face/preparation/' + photo_url.split('/')[-1], max_size=400)
                            style_path = '../../face/anime/' + choice(listdir('../../face/anime/'))
                            style = api.load_image(style_path,
                                                   max_size=400)
                            print(style_path)


                            img = api.style_transfer(content_image=content, style_image=style,
                                                     content_layer_ids=[4], style_layer_ids=[1, 2, 3],
                                                     num_iterations=29, weight_content=8, weight_style=10)

                            print(1)
                            img = Image.fromarray(np.uint8(img))
                            img.save('../../face/finished/'+photo_url.split('/')[-1])
                            print(2)

                            upload = VkUpload(vk)

                            print(
                                upload.photo_messages(photos='../../face/finished/'+photo_url.split('/')[-1]))
                            s = upload.photo_messages(photos='../../face/finished/'+photo_url.split('/')[-1])[0]['id']

                            photo = upload.photo_messages(
                                photos='../../face/finished/'+photo_url.split('/')[-1])[0]['sizes'][6]['url']
                            print(photo)


                            # vk.method('messages.send',
                            #             {'user_id': id, 'attachment': 'photo' + str(id) + '_' + str(s),
                            #             'group_id': group_id, 'random_id': obj['random_id']})

                            vk.method('messages.send',
                                      {'user_id': id, 'message': str(photo), 'random_id': obj['random_id']})


                        else:
                            vk.method('messages.send', {'user_id': id,
                                                        'message': 'Вы должны отправить только одно фото!',
                                                        'random_id': obj['random_id']})


                        ts = longPoll['ts']
                        time.sleep(0.5)

        except Exception as E:
            print(E)
            longPoll = vk.method("groups.getLongPollServer", {"group_id": group_id})
            server, key, ts = longPoll['server'], longPoll['key'], longPoll['ts']

startvk()
